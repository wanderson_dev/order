<?php

namespace OOPMentor;

use OOPMentor\OrderState\CancelledState;
use OOPMentor\OrderState\CompletedState;
use OOPMentor\OrderState\OrderStateFacade;
use OOPMentor\OrderState\PendingState;
use OOPMentor\OrderState\RefundedState;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertSame;

class OrderTest extends TestCase
{
    public function testPassingAnOrderStateFacadeToConstructorUseTheDefinedStateFacade(): void
    {
        $stateFacadeMock = $this->getMockBuilder(OrderStateFacade::class)
            ->onlyMethods(
                [
                    'createCancelledState',
                    'createCompletedState',
                    'createPendingState',
                    'createRefundedState',
                ]
            )->getMock();

        $stateFacadeMock->expects($this->once())
            ->method('createPendingState')
            ->willReturn(new PendingState());

        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            ),
            $stateFacadeMock
        );

        $stateFacadeMock->expects($this->once())
            ->method('createCompletedState')
            ->willReturn(new CompletedState());

        $order->complete();

        $stateFacadeMock->expects($this->once())
            ->method('createRefundedState')
            ->willReturn(new RefundedState());

        $order->refund();

        $stateFacadeMock->expects($this->once())
            ->method('createCancelledState')
            ->willReturn(new CancelledState());

        $order->cancel();
    }

    /**
     * @return void
     */
    #[TestDox('A new order has a pending state')]
    public function testANewOrderHasAPendingState(): void
    {
        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );

        Assert::assertInstanceOf(PendingState::class, $order->getState());
    }

    /**
     * @return void
     */
    #[TestDox('A pending order can be completed')]
    public function testAPendingOrderCanBeCompleted(): void
    {
        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );

        Assert::assertInstanceOf(PendingState::class, $order->getState());

        $order->complete();

        Assert::assertInstanceOf(CompletedState::class, $order->getState());
    }

    /**
     * @return void
     */
    #[TestDox('A pending order can be cancelled')]
    public function testAPendingOrderCanBeCancelled(): void
    {
        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );

        Assert::assertInstanceOf(PendingState::class, $order->getState());

        $order->cancel();

        Assert::assertInstanceOf(CancelledState::class, $order->getState());
    }

    /**
     * @return void
     */
    #[TestDox('A completed order can be refunded')]
    public function testACompletedOrderCanBeRefunded(): void
    {
        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );

        $order->complete();

        Assert::assertInstanceOf(CompletedState::class, $order->getState());

        $order->refund();

        Assert::assertInstanceOf(RefundedState::class, $order->getState());
    }

    /**
     * @return void
     */
    #[TestDox('An order can have zero or many products added')]
    public function testAnOrderCanHaveZeroOrManyProductsAdded(): void
    {
        $order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );

        Assert::assertEquals(0, $order->count());

        $order->add(new Product('Produto 1', 10));
        $order->add(new Product('Produto 2', 20));

        Assert::assertEquals(2, $order->count());
    }

    /**
     * @return void
     */
    #[TestDox('Getting the orders customer returns the customer set during creation')]
    public function testGettingTheOrdersCustomerReturnsTheCustomerSetDuringCreation(): void
    {
        $customer = new Customer(
            'Neto',
            new Address('123', '456'),
            new ShippingAddress('João', '456', '789')
        );

        $order = new Order($customer);

        Assert:
        assertSame($customer, $order->getCustomer());
    }
}
