<?php

namespace OOPMentor;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @return void
     */
    #[TestDox("Setters and getters should set and get the address properties")]
    public function testSettersAndGettersShouldSetAndGetTheAddressProperties(): void
    {
        $zip     = '123';
        $number  = '456';
        $address = new Address('Some zip', 'Some number');

        $address->setZip($zip);
        $address->setNumber($number);

        Assert::assertEquals($zip, $address->getZip());
        Assert::assertEquals($number, $address->getNumber());
    }
}
