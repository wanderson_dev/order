<?php

namespace OOPMentor\OrderState;

use LogicException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;

class CompletedStateTest extends AbstractOrderStateTestCase
{
    /**
     * @return void
     */
    #[TestDox('Completed order can be completed again')]
    public function testCompletedOrderCannotBeCompletedAgain(): void
    {
        $order = $this->createOrder();

        Assert::assertEquals('completed', $order->complete());
        Assert::assertInstanceOf(CompletedState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Already completed');

        $order->complete();
    }

    /**
     * @return void
     */
    #[TestDox('Completed order can be refunded')]
    public function testCompletedOrderCanBeRefunded(): void
    {
        $order = $this->createOrder();

        $order->complete();

        Assert::assertInstanceOf(CompletedState::class, $order->getState());
        Assert::assertEquals('refunded', $order->refund());
        Assert::assertInstanceOf(RefundedState::class, $order->getState());
    }

    /**
     * @return void
     */
    #[TestDox('Completed order cannot be cancelled')]
    public function testCompletedOrderCannotBeCancelled(): void
    {
        $order = $this->createOrder();

        Assert::assertEquals('completed', $order->complete());
        Assert::assertInstanceOf(CompletedState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Can\'t cancel');

        $order->cancel();
    }
}
