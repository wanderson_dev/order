<?php

namespace OOPMentor\OrderState;

use OOPMentor\Address;
use OOPMentor\Customer;
use OOPMentor\Order;
use OOPMentor\ShippingAddress;
use PHPUnit\Framework\TestCase;

abstract class AbstractOrderStateTestCase extends TestCase
{
    protected function createOrder(): Order
    {
        return new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );
    }
}
