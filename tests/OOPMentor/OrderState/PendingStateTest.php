<?php

namespace OOPMentor\OrderState;

use LogicException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;

class PendingStateTest extends AbstractOrderStateTestCase
{
    /**
     * @return void
     */
    #[TestDox('Pending order can be completed')]
    public function testPendingOrderCanBeCompleted(): void
    {
        $order = $this->createOrder();

        Assert::assertInstanceOf(PendingState::class, $order->getState());
        Assert::assertEquals('completed', $order->complete());
    }

    /**
     * @return void
     */
    #[TestDox('Pending order can be cancelled')]
    public function testPendingOrderCanBeCancelled(): void
    {
        $order = $this->createOrder();

        Assert::assertInstanceOf(PendingState::class, $order->getState());
        Assert::assertEquals('cancelled', $order->cancel());
    }

    /**
     * @return void
     */
    #[TestDox('Pending order cannot be refunded')]
    public function testPendingOrderCannotBeRefunded(): void
    {
        $order = $this->createOrder();

        Assert::assertInstanceOf(PendingState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Can\'t refund');
        $order->refund();
    }
}
