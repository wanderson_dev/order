<?php

namespace OOPMentor;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

class ShippingAddressTest extends TestCase
{
    /**
     * @return void
     */
    #[TestDox('Retrieving the receiver returns the receiver set')]
    public function testRetrievingTheReceiverReturnsTheReceiverSet(): void
    {
        $receiver = 'Neto';
        $address  = new ShippingAddress('João', '123', '456');

        $address->setReceiver($receiver);

        Assert::assertEquals($receiver, $address->getReceiver());
    }
}
