<?php

namespace OOPMentor;

/**
 * Represents a customer's address. Contains properties for the address zip code and
 * number.
 *
 * Uses composition by storing the zip code and number as private properties of the
 * class.
 *
 * The constructor accepts and assigns the zip code and number, and getter and setter
 * methods are provided to access and/or change the zip code property.
 */
class Address
{
    /**
     * Accepts and assigns the zip code and number.
     *
     * @param string $zip    The address zip code.
     * @param string $number The address number.
     */
    public function __construct(private string $zip, private string $number)
    {
    }

    /**
     * Gets the address zip code.
     *
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * Sets the address zip code.
     *
     * @param string $zip The address zip code.
     *
     * @return void
     */
    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * Get the address number.
     *
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * Set the address number.
     *
     * @param string $number The address number.
     *
     * @return void
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }
}
