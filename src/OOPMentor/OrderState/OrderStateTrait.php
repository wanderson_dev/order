<?php

namespace OOPMentor\OrderState;

use LogicException;
use OOPMentor\Order;

/**
 * Defines common methods for order state classes to implement. This trait is used
 * to avoid inheritance providing a default implementation of methods defined in the
 * `OrderState` interface.
 *
 * The trait contains three methods that correspond to the required methods in the
 * `OrderState` interface:
 *
 * * `complete()`
 * * `cancel()`
 * * `refund()`
 *
 * Each method throws a `LogicException` by default. Concrete state classes can
 * override these methods as needed to define state-specific behavior. By including
 * this trait, state classes inherit a simple implementation of the interface
 * requirements without using inheritance.
 */
trait OrderStateTrait
{
    /**
     * The complete() method. Throws a LogicException by default.
     *
     * @param Order $order The order instance.
     *
     * @return string
     * @throws LogicException
     */
    public function complete(Order $order): string
    {
        throw new LogicException('Can\'t complete');
    }

    /**
     * The cancel method. Throws a LogicException by default.
     *
     * @param Order $order The order instance.
     *
     * @return string
     * @throws LogicException
     */
    public function cancel(Order $order): string
    {
        throw new LogicException('Can\'t cancel');
    }

    /**
     * The refund method. Throws a LogicException by default.
     *
     * @param Order $order The order instance.
     *
     * @return string
     * @throws LogicException
     */
    public function refund(Order $order): string
    {
        throw new LogicException('Can\'t refund');
    }
}
