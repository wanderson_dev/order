<?php

namespace OOPMentor\OrderState;

use LogicException;
use OOPMentor\Order;

/**
 * Represents the "refunded" state of an Order. Once an order has been refunded, it
 * cannot be refunded again. Implements the OrderState interface to define the behavior
 * of a refunded order.
 *
 * Uses the `OrderStateTrait` trait to inherit common methods.
 *
 * * The `refund()` method throws a `LogicException`, since an order cannot be refunded
 * multiple times.
 * * The `cancel()` method changes the order state to a cancelled state, allowing a
 * refunded order to then be cancelled.
 */
class RefundedState implements OrderState
{
    use OrderStateTrait;

    /**
     * Throws a LogicException if called, to prevent invalid attempts to refund an
     * order that is already refunded.
     *
     * @param Order $order The order being refunded.
     *
     * @return string
     * @throws LogicException
     */
    public function refund(Order $order): string
    {
        throw new LogicException('Already refunded');
    }

    /**
     * Changes the order state to a cancelled state, allowing a refunded order to
     * then be cancelled.
     *
     * @param Order $order The order being cancelled.
     *
     * @return string
     */
    public function cancel(Order $order): string
    {
        $order->setState($order->getStateFacade()->createCancelledState());

        return 'cancelled';
    }
}
