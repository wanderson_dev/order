<?php

namespace OOPMentor\OrderState;

use LogicException;
use OOPMentor\Order;

/**
 * Represents the "cancelled" state of an Order. An order that is in the
 * cancelled state cannot be cancelled again. Implements the OrderState interface to
 * define the behavior of a cancelled order. Also uses the OrderStateTrait trait to
 * inherit common methods for order state classes without using inheritance for this.
 * This trait likely defines a default implementation for methods on the OrderState
 * interface.
 *
 * * The sole method `cancel()` throws a `LogicException` if called on a cancelled
 * order, to prevent invalid cancellation attempts on orders already in the cancelled
 * state.
 **/
class CancelledState implements OrderState
{
    use OrderStateTrait;

    /**
     * Throws a LogicException if called, since an order cannot be cancelled once
     * it has already reached the cancelled state.
     *
     * @param Order $order The order being cancelled.
     *
     * @return string
     * @throws LogicException When attempting to cancel an already cancelled order.
     */

    public function cancel(Order $order): string
    {
        throw new LogicException('Already cancelled');
    }
}
