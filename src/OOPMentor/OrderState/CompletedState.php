<?php

namespace OOPMentor\OrderState;

use LogicException;
use OOPMentor\Order;

/**
 * Represents the "completed" state of an Order. Once an order reaches the completed
 * state, it can no longer be completed again. Implements the OrderState interface to
 * define the behavior of a completed order.
 * Uses the OrderStateTrait trait to inherit common methods for order state classes.
 *
 * * The `complete()` method throws a `LogicException` if called, since an order cannot be
 * completed once it is already in the completed state.
 * * The `refund()` method changes the order's state to a refunded state.
 */
class CompletedState implements OrderState
{
    use OrderStateTrait;

    /**
     * Throws a LogicException if called, to prevent invalid attempts to complete an
     * order that is already completed.
     *
     * @param Order $order The order being completed.
     *
     * @return string
     * @throws LogicException When attempting to complete an order that is already
     * completed.
     */
    public function complete(Order $order): string
    {
        throw new LogicException('Already completed');
    }

    /**
     * Changes the order state to a refunded state.
     *
     * @param Order $order The order being refunded.
     *
     * @return string
     */
    public function refund(Order $order): string
    {
        $order->setState($order->getStateFacade()->createRefundedState());

        return 'refunded';
    }
}
