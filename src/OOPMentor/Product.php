<?php

namespace OOPMentor;

/**
 * Represents a product in the store.
 */
class Product
{
    /**
     * The product's name.
     *
     * @var string
     */
    private string $name;

    /**
     * The product's price.
     *
     * @var float
     */
    private float $price;

    /**
     * Creates a new product with its name and price.
     *
     * @param string $name  The product's name.
     * @param float  $price The product's price.
     */
    public function __construct(string $name, float $price)
    {
        $this->setName($name);
        $this->setPrice($price);
    }

    /**
     * Gets the product's name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the product's name.
     *
     * @param string $name The product's name.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets the product's price.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Sets the product's price.
     *
     * @param float $price The product's price.
     *
     * @return void
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
}
